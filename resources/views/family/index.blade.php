@include('_header')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                {{trans('messages.list')}}
            </h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            {{trans('messages.family')}}
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <a href="{{url('families/add')}}" class="btn btn-info btn-sm">{{trans('menu.add_family')}}</a>
                            <a href="{{url('families')}}" class="btn btn-success btn-sm">{{trans('menu.list_family')}}</a>
                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>{{trans('messages.name')}}</th>
                                    <th>{{trans('messages.address')}}</th>
                                    <th>{{trans('messages.created_at')}}</th>
                                    <th>{{trans('messages.updated_at')}}</th>
                                    <th>{{trans('messages.option')}}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>{{trans('messages.name')}}</th>
                                    <th>{{trans('messages.address')}}</th>
                                    <th>{{trans('messages.created_at')}}</th>
                                    <th>{{trans('messages.updated_at')}}</th>
                                    <th>{{trans('messages.option')}}</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @if(!empty($data))
                                    @foreach($data as $d)
                                        <tr>
                                            <td>{{$d->name}}</td>
                                            <td>{{$d->address}}</td>
                                            <td>{{$d->created_at}}</td>
                                            <td>{{$d->updated_at}}</td>
                                            <td>
                                                <a href="{{url('families/edit/'.$d->id)}}" class="btn btn-warning btn-xs">
                                                    {{trans('menu.edit')}}
                                                </a>

                                                <a href="#myModal"  class="btn btn-danger btn-xs xx" data-id="{{$d->id}}" data-toggle="modal" onclick="teste('{{$d->id}}', '{{$d->name}}')">
                                                    {{trans('menu.delete')}}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        <!-- Modal -->
        <div class="modal fade" name="myModal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <form method="post" action="{{url('families/delete')}}">
                        <div class="modal-body">
                            <p>Deseja apagar o registro?</p>
                            <div id="name"></div>
                            <input type="hidden" name="id"  id="id" readonly>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info">{{trans('menu.save')}}</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('menu.cancel')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function teste(val, name) {
        $("#id").val(val);
        $("#name").html('<strong>' + name + '</strong>');
    }
</script>
@include('_footer')