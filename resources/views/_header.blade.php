<!doctype html>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="5Solas Church Admin">
    <meta name="author" content="Seyr Lemos">

    <title>{{trans('messages.title')}}</title>
    
    {!! Charts::assets() !!}

    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{URL::asset('template/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{URL::asset('template/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{URL::asset('template/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{URL::asset('template/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="{{URL::asset('template/plugins/waitme/waitMe.css')}}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{URL::asset('template/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{URL::asset('template/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{URL::asset('template/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{URL::asset('template/css/themes/all-themes.css')}}" rel="stylesheet" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.js"></script>


    <!-- JQuery -->
    <script src="{{URL::asset('template/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('js/combobox.js')}}"></script>
    <script src="{{URL::asset('js/jquery.mask.min.js')}}"></script>
  
    
    <!-- Include this after the sweet alert js file -->
    <script src="{{URL::asset('js/sweetalert.min.js')}}"></script>
    <link rel="stylesheet" href="{{URL::asset('css/sweetalert.css')}}">


    <!-- new fields html5 firefox-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webshim/1.15.10/dev/polyfiller.js"></script>
    

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="theme-blue">
@include('sweet::alert')

    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>  
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="{{url('/dashboard')}}">FIVE SOLAS CHURCH ADMIN</a>
            </div>
        </div>
    </nav>

    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{url('/')}}/img/ts-avatar.jpg" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                    <div class="email">{{Auth::user()->email}}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ url('/account') }}"><i class="material-icons">account_circle</i>{{ trans('menu.profile') }}</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">assignment_ind</i>{{ trans('menu.edit_profile') }}</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="{{ url('/logout') }}"><i class="material-icons">input</i>{{ trans('menu.logout') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->

            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">{{ trans('menu.main') }}</li>
                    <li>
                        <a href="{{URL('/dashboard')}}">
                            <i class="material-icons">spa</i>
                            <span>{{ trans('menu.dashboard') }}</span>
                        </a>
                    </li>
                    <!--family menu-->
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">wc</i>
                            <span>{{ trans('menu.families') }}</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('/families')}}">{{ trans('menu.list_family') }}</a>
                            </li>
                            <li>
                                <a href="{{url('/members')}}">{{ trans('menu.list_member') }}</a>
                            </li>
                        </ul>
                    </li>

                    <!--tithe menu-->
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">local_atm</i>
                            <span>{{ trans('menu.tithes') }}</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('/tithes')}}">{{ trans('menu.add_tithe') }}</a>
                            </li>
                            <li>
                                <a href="{{url('/types')}}">{{ trans('menu.add_type_tithe') }}</a>
                            </li>
                        </ul>
                    </li>

                    <!-- expense menu-->
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">attach_money</i>
                            <span>{{ trans('menu.expenses') }}</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('/accounts')}}">{{ trans('menu.add_account') }}</a>
                            </li>
                            <li>
                                <a href="{{url('/categories')}}">{{ trans('menu.category') }}</a>
                            </li>
                            <li>
                                <a href="{{url('/expenses')}}">{{ trans('menu.expense') }}</a>
                            </li>
                        </ul>
                    </li>

                    <!-- report menu -->
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">pie_chart</i>
                            <span>{{ trans('menu.charts') }}</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('reports/tithesmember')}}">{{ trans('menu.tithes') }}</a>
                            </li>
                            <li>
                                <a href="{{url('reports/tithesbymonth')}}">{{ trans('menu.tithes_graph') }}</a>
                            </li>
                            <li>
                                <a href="{{url('reports/expenses')}}">{{ trans('menu.expenses') }}</a>
                            </li>
                            <li>
                                <a href="{{url('reports/expensegraph')}}">{{ trans('menu.expenses_graph') }}</a>
                            </li>
                        </ul>
                    </li>

                    <!-- administration menu-->
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">build</i>
                            <span>{{ trans('menu.administration') }}</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{url('/users')}}">{{ trans('menu.users') }}</a>
                            </li>
                            <li>
                                <a href="{{url('/entities')}}">{{ trans('menu.entities') }}</a>
                            </li>
                        </ul>
                    </li>
                    
                    
                    <li>
                        <a href="{{URL('/logout')}}">
                            <i class="material-icons">input</i>
                            <span>{{trans('menu.logout')}}</span>
                        </a>
                    </li>
                    
            </div>
            <!-- #Menu -->

            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; {{date('Y')}} <a href="http://seyrlemos.wordpress.com">Five Solas Church Admin</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0
                </div>
                <div class="copyright">
                    Template by AdminBSB - Material Design.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
