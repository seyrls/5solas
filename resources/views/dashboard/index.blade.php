@include('_header')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                {{trans('messages.dashboard')}}
            </h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">wc</i>
                    </div>
                    <div class="content">
                        <div class="text">{{trans('messages.members')}}</div>
                        <div class="number count-to">
                            {{$member}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">account_balance_wallet</i>
                    </div>
                    <div class="content">
                        <div class="text">{{trans('messages.balances')}} : {{date('F , Y')}}</div>
                        <div class="number count-to">{{trans('forms.symbol_money') . number_format($balance,2)}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">attach_money</i>
                    </div>
                    <div class="content">
                        <div class="text">{{trans('messages.amount_expenses')}} / {{date('F , Y')}}</div>
                        <div class="number count-to">{{trans('forms.symbol_money') . number_format($expense,2)}}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">local_atm</i>
                    </div>
                    <div class="content">
                        <div class="text">{{trans('messages.amount_tithes')}} / {{date('F , Y')}}</div>
                        <div class="number count-to">{{trans('forms.symbol_money') . number_format($tithes,2)}}</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->

        <!-- graphs -->
        <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2>{{trans('messages.amount_expenses')}} / {{date('Y')}}</h2>
                    </div>
                    <div class="body">
                        <div>
                            {!! $expenses->render() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                <div class="card">
                    <div class="header">
                        <h2>{{trans('messages.amount_tithes') . "/" . date('Y')}}</h2>
                    </div>
                    <div class="body">
                        <div>
                            {!!($chart->render())!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('_footer')