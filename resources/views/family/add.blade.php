@include('_header')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                {{trans('messages.list')}}
            </h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            {{trans('messages.family')}}
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <a href="{{url('families/add')}}" class="btn btn-info btn-sm">{{trans('menu.add_family')}}</a>
                            <a href="{{url('families')}}" class="btn btn-success btn-sm">{{trans('menu.list_family')}}</a>
                        </ul>
                    </div>
                    <div class="body">    
                        <form method="post" action="{{url('families/save')}}">
                            <label for="family_name">{{trans('forms.family_name')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="name" type="text" class="form-control" placeholder="{{trans('forms.family_name')}}" required>
                                </div>
                            </div>
                            <label for="password">{{trans('forms.address')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="address" type="text" class="form-control" placeholder="{{trans('forms.address')}}">
                                </div>
                            </div>

                            <br>
                            <button class="btn btn-info m-t-15 waves-effect" type="submit">{{trans('menu.save')}}</button>
                            <button class="btn btn-danger m-t-15 waves-effect" type="reset">{{trans('menu.cancel')}}</button>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_footer')