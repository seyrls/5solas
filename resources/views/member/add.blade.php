@include('_header')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                {{trans('messages.list')}}
            </h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            {{trans('messages.member')}}
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <a href="{{url('members/add')}}" class="btn btn-info btn-sm">{{trans('menu.add_member')}}</a>
                            <a href="{{url('members')}}" class="btn btn-success btn-sm">{{trans('menu.list_member')}}</a>
                        </ul>
                    </div>
                    <div class="body">    
                        <form method="post" action="{{url('members/save')}}">

                            <label for="family_name">{{trans('forms.family_name')}}</label>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <select class="form-control show-tick" name="family_id">
                                        @foreach($data as $d)
                                            <option value="{{$d->id}}">{{$d->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <label for="name">{{trans('forms.member_name')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="name" type="text" class="form-control" placeholder="{{trans('forms.member_name')}}" required>
                                    
                                </div>
                            </div>

                            <label for="birthday">{{trans('forms.birthday')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="birthday" type="date" class="form-control">
                                </div>
                            </div>

                            <label for="telephone">{{trans('forms.telephone')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="telephone" id="telephone" type="text" class="form-control" placeholder="{{trans('forms.phone_placeholder')}}">
                                </div>
                            </div>

                            <label for="cellphone">{{trans('forms.cellphone')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="cellphone" id="cellphone" type="text" class="form-control" placeholder="{{trans('forms.phone_placeholder')}}">
                                </div>
                            </div>

                            <label for="email">{{trans('forms.email')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="email" type="email" class="form-control" placeholder="{{trans('forms.email_placeholder')}}">
                                </div>
                            </div>

                            <label for="gender">{{trans('forms.gender')}}</label>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <select class="form-control show-tick" name="gender">
                                        <option value="m">{{trans('forms.male')}}</option>
                                        <option value="f">{{trans('forms.female')}}</option>
                                    </select>
                                </div>
                            </div>

                            <label for="image">{{trans('forms.image')}}</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input name="image" type="file">
                                </div>
                            </div>

                            <input type="hidden" name="status" value="1">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <br>
                            <button class="btn btn-info m-t-15 waves-effect" type="submit">{{trans('menu.save')}}</button>
                            <button class="btn btn-danger m-t-15 waves-effect" type="reset">{{trans('menu.cancel')}}</button>

                            
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('_footer')